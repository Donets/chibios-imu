/*
 * i2c.h
 *
 *  Created on: 12 мая 2015 г.
 *      Author: Administrator
 */

#ifndef SYSTEM_INC_I2C_LOC_H_
#define SYSTEM_INC_I2C_LOC_H_

#define I2C_DRIVER  I2CD1
#define I2C_SEND i2cMasterTransmitTimeout
#define TIMEOUT MS2ST(1000)

void I2C1_Init();
void HMC5883L_I2C_BytesWrite(uint8_t slaveAddr, uint8_t* txBuffer);
uint8_t HMC5883L_I2C_BytesRead(uint8_t slaveAddr, uint8_t* txBuffer);
void HMC5883L_I2C_DataRead(uint8_t slaveAddr, uint8_t bytesNum);

#endif /* SYSTEM_INC_I2C_LOC_H_ */
